#pragma once

// Use only by Glue Applications ! //
#include "Glue/Application.h"
#include "Glue/Layer.h"
#include "Glue/Log.h"

#include "Glue/Input.h"
#include "Glue/KeyCodes.h"
#include "Glue/MouseButtonCodes.h"

#include "Glue/ImGui/ImGuiLayer.h"

// ----- ENTRY POINT ---- //
#include "Glue/EntryPoint.h"